 $(document).ready(function(){
	function t(t) {
		var e, n = ["webkit", "Moz", "ms", "o"],
			a = [],
			o = document.documentElement.style,
			s = function(t) {
				return t.replace(/-(\w)/g, function(t, e) {
					return e.toUpperCase()
				})
			};
		for (e in n) a.push(s(n[e] + "-" + t));
		a.push(s(t));
		for (e in a)
			if (a[e] in o) return !0;
		return !1
	}
	var e = $(window),
		n = e.height(),
		a = e.width(),
		o = $(document).height(),
		s = !1,
		i = sessionStorage.getItem("section") || 0,
		c = function(t) {
			return 0 > t ? i++ : i--, 0 > i && (i = 0), i > 6 && (i = 6), i
		};
	var wd=$(window).width(),
	    scale = wd / 1440,
	    tr = $(".tans");
	if( wd >= 1440 ){
		tr.css("transform", "scale(1)");
	}else if( wd >= 1360 && wd < 1440 ){
		tr.css("transform", "scale(" + scale + ")");
		$(".logobox.tans").css("transform-origin", "0 0");
		$(".cenbox.tans").css("transform-origin", "40% 0");
	}else if( wd >= 1200 && wd < 1360 ){
		tr.css("transform", "scale(" + scale + ")");
		$(".logobox.tans").css("transform-origin", "150% 0");
		$(".cenbox.tans").css("transform-origin", "50% 0");
        $(".inbox").css("height", "800");
	}else if( wd >= 1100 && wd < 1200 ){
		tr.css("transform", "scale(" + scale + ")");
		$(".logobox.tans").css("transform-origin", "150% 0");
		$(".cenbox.tans").css("transform-origin", "30% 0");
		$(".inbox").css("height", "720");
	}else if( wd >= 1000 && wd < 1100 ){
		tr.css("transform", "scale(" + scale + ")");
		$(".logobox.tans").css("transform-origin", "120% 0");
		$(".cenbox.tans").css("transform-origin", "28% 0");
		$(".inbox").css("height", "620");
	}else if( wd >= 888 && wd < 1000 ){
		tr.css("transform", "scale(" + scale + ")");
		$(".logobox.tans").css("transform-origin", "80% 0");
		$(".cenbox.tans").css("transform-origin", "18% 0");
		$(".inbox").css("height", "560");
	}else if( wd > 750 && wd < 888 ){
		tr.css("transform", "scale(" + scale + ")");
		$(".logobox.tans").css("transform-origin", "50% 0");
		$(".cenbox.tans").css("transform-origin", "0 0");
		$(".inbox").css("height", "470");
	}
	else{
		var scale = wd / 750;
		tr.css("transform", "1");
	}
	$(window).resize(function() {
	  	var wd=$(window).width(),
	    scale = wd / 1440;
	    if( wd >= 1440 ){
			tr.css("transform", "scale(1)");
		}else if( wd >= 1360 && wd < 1440 ){
			tr.css("transform", "scale(" + scale + ")");
			$(".logobox.tans").css("transform-origin", "0 0");
			$(".cenbox.tans").css("transform-origin", "40% 0");
		}else if( wd >= 1200 && wd < 1360 ){
			tr.css("transform", "scale(" + scale + ")");
			$(".logobox.tans").css("transform-origin", "150% 0");
			$(".cenbox.tans").css("transform-origin", "50% 0");
			$(".inbox").css("height", "800");
		}else if( wd >= 1100 && wd < 1200 ){
			tr.css("transform", "scale(" + scale + ")");
			$(".logobox.tans").css("transform-origin", "150% 0");
			$(".cenbox.tans").css("transform-origin", "30% 0");
			$(".inbox").css("height", "720");
		}else if( wd >= 1000 && wd < 1100 ){
			tr.css("transform", "scale(" + scale + ")");
			$(".logobox.tans").css("transform-origin", "120% 0");
			$(".cenbox.tans").css("transform-origin", "28% 0");
			$(".inbox").css("height", "620");
		}else if( wd >= 888 && wd < 1000 ){
			tr.css("transform", "scale(" + scale + ")");
			$(".logobox.tans").css("transform-origin", "80% 0");
			$(".cenbox.tans").css("transform-origin", "18% 0");
			$(".inbox").css("height", "560");
		}else if( wd > 750 && wd < 888 ){
			tr.css("transform", "scale(" + scale + ")");
			$(".logobox.tans").css("transform-origin", "50% 0");
			$(".cenbox.tans").css("transform-origin", "0 0");
			$(".inbox").css("height", "470");
		}
		else{
			var scale = wd / 750;
			tr.css("transform", "scale(1)");
		}
	});
});
// function fbFuc() {
// 	var getw = $(".cenbox .phone").width();
// 	// var fb = $('.wave').height();
// 	// $(".section").css("padding-bottom", fb + 45 + "px");
// 	$(".cenbox .phone").css("height", 0.9*getw + 15 + "px");
// }