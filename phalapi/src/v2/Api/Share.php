<?php
namespace V2\Api;

use PhalApi\Api;

/**
 * 分享内容
 *
 * @author: cwg
 */

class Share extends Api
{
    public function getRules()
    {
        return array(
          'index' => array(
          ),
      );
    }

    /**
     * 分享内容
     * @desc  分享内容
     * @return string title  分享标题
     * @return string url  分享URL
     * @return string text  分享描述
     */
    public function index()
    {
        $data['title']='多开神器';
        $data['text']='大号小号工作号一部手机多个帐号同时登陆！';
        $data['url']='http://wxfs.181613.com/';
        return $data;
    }
}
