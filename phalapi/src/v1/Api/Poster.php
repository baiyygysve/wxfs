<?php
namespace V1\Api;

use PhalApi\Api;
use V1\Domain\Controlad as DomainControlad;

/**
 * 广告控制
 *
 * @author: cwg
 */

class Poster extends Api
{
    public function getRules()
    {
        return array(
          '*' => array(
               'partner' => array('name' => 'partner', 'type' => 'string', 'require' => true, 'desc' => '渠道商标识','source'=>'post'),
               'versioncode'=>array('name' => 'versioncode', 'type' => 'int', 'require' => true, 'desc' => '版本号','source'=>'post'),
           ),

           'postHongbaoControl'=>array(
             'ids'=>array('name'=>'ids','type' => 'array', 'default' => '0','format' => 'explode','desc'=>'已点击过的广告ID','source'=>'post'),
           ),
      );
    }
    //'format' => 'json/explode'
    /**
     * 整个app广告接口
     * @desc 整个APP广告是否显示，包含联盟广告与自身推广广告
     * @return int show 0隐藏 1显示
     */
    public function isShow()
    {
        $domain = new DomainControlad();
        $channel=$this->partner;
        $versioncode=$this->versioncode;
        $closeversioncode = intval($domain->getversioncode($channel));
        //$closeversioncode为0全部关闭
        if ($closeversioncode==0) {
            $rs['show']=0;//关闭显示
            return $rs;
        }
        //当前版本号小于服务端存储关闭广告的版本号，显示广告，否则开启广告
        if ($versioncode<$closeversioncode) {
            $rs['show']=1;
        } else {
            $rs['show']=0;//关闭显示
        }
        return $rs;
    }
    /**
  * 控制开机广告
  * @desc 控制开机广告
  * @return int show 0隐藏 1展示服端广告 2展示联盟广告
  * @return string url 服务端广告URL
  * @return string imageurl 服务端广告图片地址
  * @return int id 广告id
  */
    public function postBootControl()
    {
        $versioncode=$this->versioncode;
        $channel=$this->partner;
        $domain = new DomainControlad();
        $data=$domain->postBootControl($channel, $versioncode);
        return $data;
    }
    /**
  * 控制app广告
  * @desc 控制app广告
  * @return int show 0隐藏 1
  * @return int time 广告轮播时间间隔单位为毫秒
  * @return array list 数组
  * @return string url 服务端广告URL
  * @return string imageurl 服务端广告图片地址
  * @return string content 应用广告参数
  * @return string title 标题
  * @return bool wxxct 是否徾信小程序 true 是 false 否
  * @return int id 广告id
  * @return string from 徾信小程序推广来源标识
  */
    public function postAppControl()
    {
        $versioncode=$this->versioncode;
        $channel=$this->partner;
        $domain = new DomainControlad();
        $data=$domain->postAppControl($channel, $versioncode);
        return $data;
    }
    /**
  * 控制banner广告
  * @desc 控制banner广告
  * @return int show 0隐藏 1展示服端广告 2展示联盟广告
  * @return string url 服务端广告URL
  * @return string imageurl 服务端广告图片地址
  * @return int id 广告id
  */
    public function postBannerControl()
    {
        $versioncode=$this->versioncode;
        $channel=$this->partner;
        $domain = new DomainControlad();
        $data=$domain->postBannerControl($channel, $versioncode);
        return $data;
    }
    /**
  * 控制红包广告
  * @desc 控制红包广告
  * @return int show 0隐藏 1展示广告
  * @return string url 服务端广告URL
  * @return string imageurl 服务端广告图片地址
  * @return string content 复制的文本
  * @return int id 广告id
  */
    public function postHongbaoControl()
    {
        $ids=$this->ids;
        $versioncode=$this->versioncode;
        $channel=$this->partner;
        $domain = new DomainControlad();
        $data=$domain->postHongbaoControl($channel, $versioncode, $ids);
        return $data;
    }
}
