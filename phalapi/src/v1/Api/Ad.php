<?php
namespace V1\Api;

use PhalApi\Api;
use V1\Domain\Ad as Domainad;
use V1\Domain\Adcount as Domainadcount;

/**
 * 点击广告
 *
 * @author: cwg
 */

class Ad extends Api
{
    public function getRules()
    {
        return array(
          'hit' => array(
            'id' =>array('name'=>'id','type'=>'int','require'=>true,'desc'=>'id值','source'=>'post'),
            'adtype' =>array('name'=>'adtype','type'=>'int','require'=>true,'desc'=>'1开机图，2 banner,3 红包','source'=>'post'),
           ),
      );
    }
    /**
  * 控制红包广告
  * @desc 控制红包广告
  * @return string|bool url 服务端广告URL
  */
    public function Hit()
    {
        $adtype=$this->adtype;
        $id = intval($this->id);
        if ($id==0) {
            $data['url']=false;
            return $data;
        }
        $domain = new Domainad();
        $ad = $domain->findById($id);
        $imgurl = isset($ad['imgkurl']) ? $ad['imgkurl'] : '';
        $ad_id = isset($ad['ad_id']) ? $ad['ad_id'] : '';
        $downurl = isset($ad['downurl']) ? $ad['downurl'] : '';
        $domain = new Domainadcount();
        if ($ad_id) {
            $domain->addCount($adtype, 2, $imgurl, $ad_id, $downurl);//点击
        }
        $url=htmlspecialchars_decode($ad['downurl']);
        $data['url']=$url;
        return $data;
        // header('Location: '.$url);
    }
}
