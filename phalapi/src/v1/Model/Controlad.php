<?php
namespace V1\Model;

use PhalApi\Model\NotORMModel as NotORM;

class Controlad extends NotORM
{
    public function doSth()
    {
        return $this->getORM();
    }
    protected function getTableName($id)
    {
        return 'control_ad';
    }
    /**
     * 根据条件查询广告列表
     * @param $type
     * @return mixed
     */
    public function getversioncode($channel)
    {
        $data = $this->doSth()->where('channel', $channel)->select('versioncode')->fetchOne();
        if (!$data) {
            $versioncode=100000000;
        } else {
            $versioncode=$data['versioncode'];
        }
        return $versioncode;
    }
    /**
     * 根据条件查询广告列表
     * @param $type
     * @return mixed
     */
    public function getList()
    {
        $list = $this->doSth()->where('status', 1)->select('channel')->fetchAll();
        $result = array_column($list, 'channel');
        return $result;
    }
    /**
     * 根据条件查询服务端控制情况　后台开机图广告展示开关打开展示后台自定义广告，不显示联盟广告，0关所有广告，1展示服端广告，2展示联盟广告
     * @param $type
     * @return array bootad 开机广告控制情况
     */
    public function getBootControl($channel)
    {
        $data = $this->doSth()->where('channel', $channel)->select('bootad')->fetchOne();
        if (!$data) {
            //没有记录默认走联盟广告
            $data['bootad']=2;
        }
        return $data['bootad'];
    }
    /**
        * 根据条件查询服务端控制情况　后台banner图广告展示开关打开展示后台自定义广告，不显示联盟广告，0关所有广告，1展示服端广告，2展示联盟广告
        * @param $type
        * @return array bannerad 开机广告控制情况
        */
    public function getBannerControl($channel)
    {
        $data = $this->doSth()->where('channel', $channel)->select('bannerad')->fetchOne();
        if (!$data) {
            //没有记录默认走联盟广告
            $data['bannerad']=2;
        }
        return $data['bannerad'];
    }
    /**
     * APP广告位广告
     * @param $type
     * @return array appad 广告控制情况
     */
    public function getAppControl($channel)
    {
        $data = $this->doSth()->where('channel', $channel)->select('appad')->fetchOne();
        if (!$data) {
            //没有记录默认打开广告
            $data['appad']=1;
        }
        return $data['appad'];
    }
    /**
     * 根据条件查询服务端控制情况　后台hongbaoad广告展示开关情况，0关所有广告，1显示广告
     * @param $type
     * @return array bannerad 开机广告控制情况
     */
    public function getHongbaoControl($channel)
    {
        $data = $this->doSth()->where('channel', $channel)->select('hongbaoad')->fetchOne();
        if (!$data) {
            //没有记录默认打开
            $data['hongbaoad']=1;
        }
        return $data['hongbaoad'];
    }
}
