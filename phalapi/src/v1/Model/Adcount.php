<?php
namespace V1\Model;

use PhalApi\Model\NotORMModel as NotORM;

class Adcount extends NotORM
{
    public function doSth()
    {
        return $this->getORM();
    }
    protected function getTableName($id)
    {
        return 'adcount';
    }
    //添加数据统计
    public function addCount($data)
    {
        $count = $this->doSth()->insert($data);
        return $count;
    }

    //查询
    public function findCount($add_time, $ad_id)
    {
        $info = $this->doSth()->select('id')->where(array('addtime'=>$add_time,'ad_id'=>$ad_id))->fetchOne();
        return $info;
    }

    public function updateNum($add_time, $status, $ad_id)
    {
        if ($status==1) {
            $num= $this->doSth()->where(array('addtime'=>$add_time,'ad_id'=>$ad_id))->update(array('show_num' => new \NotORM_Literal("show_num+1")));
        } else {
            $num= $this->doSth()->where(array('addtime'=>$add_time,'ad_id'=>$ad_id))->update(array('click_num' => new \NotORM_Literal("click_num+1")));
        }
        return $num;
    }
}
