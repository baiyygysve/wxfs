<?php
namespace V1\Model;

use PhalApi\Model\NotORMModel as NotORM;

class Ad extends NotORM
{
    public function doSth()
    {
        return $this->getORM();
    }
    protected function getTableName($id)
    {
        return 'ad';
    }
    /**
     * 根据条件查询广告列表
     * @param $type
     * @return mixed
     */
    public function getList($type)
    {
        $list = $this->doSth()->where('type', $type)->where('status', 1)->select('ad_id,downurl,imgkurl,content,title')->fetchAll();
        return $list;
    }

    /**
     * 根据ID查询广告
     * @param $id
     * @return mixed
     */
    public function findById($id)
    {
        $info = $this->doSth()->where('ad_id', $id)->where('status', 1)->select('ad_id,downurl,imgkurl')->fetchOne();
        return $info;
    }
}
