<?php
namespace V1\Domain;

use V1\Model\Adcount as Modeladcount;

class Adcount
{
    public function addCount($adtype, $status, $imgurl, $ad_id, $downurl)
    {
        $add_time = strtotime(date("Y-m-d", SYS_TIME));
        $model=new Modeladcount();
        $info=$model->findCount($add_time, $ad_id);
        if ($info) {
            $model->updateNum($add_time, $status, $ad_id);
        } else {
            $data['adtype'] = $adtype;
            $data['addtime'] = $add_time;
            $data['thumb'] = $imgurl;
            $data['ad_id'] = $ad_id;
            $data['linkurl'] = $downurl;
            if ($status==1) {
                $data['show_num']=1;
            } else {
                $data['click_num']=1;
            }
            $id=$model->addCount($data);
            if (!$id) {
                return false;
            }
        }
        return true;
    }
}
