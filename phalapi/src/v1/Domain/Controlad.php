<?php
namespace V1\Domain;

use V1\Model\Controlad as Modelcontrolad;
use V1\Domain\Ad as Domainad;
use V1\Domain\Adcount as Domainadcount;

class Controlad
{
    public function getversioncode($channel)
    {
        $key=md5('channel_versioncode_compare'.$channel);
        $versioncode=\PhalApi\DI()->cache->get($key);
        if (!$versioncode) {
            $model=new Modelcontrolad();
            $versioncode = $model->getversioncode($channel);
            \PhalApi\DI()->cache->set($key, $versioncode, 60);
        }
        return $versioncode;
    }
    /**
    * @auth chenwengen
    * @data 20180628
    * @description 通用开机图广告控制 0关闭所有广告 1展示后台广告，2展示联盟广告 马甲1会员关闭所有广告
    *@param $channel 渠道好号
    *@param $versioncode 版本号
    *@return url 后台广告url
    *@return imageurl 后台广告图片url
    *@return show 广告类型 0关闭所有广告 1展示后台广告，2展示联盟广告
    **/
    public function postBootControl($channel, $versioncode)
    {
        $data['id']=0;
        $data['url']='';
        $data['imageurl']='';
        $data['show']=2;
        $versioncode=intval($versioncode);
        // 情况2　渠道上线关广告 判断渠道上线配置是否配置了当前版本上线关广告的功能配置了，配置了关闭所有广告
        $offversioncode=$this->getversioncode($channel);
        //当前版本号大等于服务端存储关闭广告的版本号关闭广告 为0关闭所有广告
        if ($versioncode>=$offversioncode || $offversioncode=='0') {
            $data['show']=0;
            return $data;
        }
        // 情况３　查询开机图广告控制是否是展示后台广告还是展示联盟广告，还是全量关闭广告，广告数据一起返回，如果开通联盟广告关闭的话可以展示后台的广告
        //查询后台广告控制情况
        $model=new Modelcontrolad();
        //bootstatus 0关所有广告，1展示服端广告，2展示联盟广告
        $bootstatus=$model->getBootControl($channel);
        $data['show']=$bootstatus;
        //展示服务端广告的或者联盟广告的时候查询库
        //'adtype' =>array('name'=>'adtype','range'=>array(1,2),'require'=>true,'desc'=>'1开机图，2 banner
        $adtype = 1;
        $domain_ad= new Domainad();
        $adsarr = $domain_ad->getList($adtype);
        if (empty($adsarr)) {
            //所有服务端广告关闭了，或者没有服务端广告，直接默认关闭所有广告
            $data['show']=2;
            return $data;
        }
        $total=count($adsarr)-1;
        $adskey=rand(0, $total);
        $list=$adsarr[$adskey];
        $id = $list['ad_id'];
        $data['id']=$id;
        $data['url']  = "http://www.wxfs181613.com/index.php?service=V1.Ad.hit&id=$id&adtype=$adtype";
        $imgurl = isset($list['imgkurl']) ? $list['imgkurl'] : '';
        $downurl = isset($list['downurl']) ? $list['downurl'] : '';
        $data['imageurl']=$imgurl;
        $domain_adcount =new Domainadcount();
        //展示后台广告的时候才统计显示数
        if ($id && $data['show']==1) {
            $domain_adcount->addCount($adtype, 1, $imgurl, $id, $downurl);//1展示
        }
        return $data;
    }
    /**
    * @auth chenwengen
    * @data 20180628
    * @description 通用banner广告控制 0关闭所有广告 1展示后台广告，2展示联盟广告 马甲1会员关闭所有广告
    *@param $channel 渠道好号
    *@param $versioncode 版本号
    *@return url 后台广告url
    *@return imageurl 后台广告图片url
    *@return show 广告类型 0关闭所有广告 1展示后台广告，2展示联盟广告
    **/
    public function postBannerControl($channel, $versioncode)
    {
        $data['id']=0;
        $data['url']='';
        $data['imageurl']='';
        $data['show']=2;
        // 情况2　渠道上线关广告 判断渠道上线配置是否配置了当前版本上线关广告的功能配置了，配置了关闭所有广告
        $offversioncode=$this->getversioncode($channel);
        //当前版本号大等于服务端存储关闭广告的版本号关闭广告 为0关闭所有广告
        if ($versioncode>=$offversioncode || $offversioncode=='0') {
            $data['show']=0;
            return $data;
        }

        // 情况３　查询banner广告控制是否是展示后台广告还是展示联盟广告，告数据一起返回，如果开通联盟广告关闭的话可以展示后台的广告
        //查询后台广告控制情况
        $model=new Modelcontrolad();
        //bootstatus 0关所有广告，1展示服端广告，2展示联盟广告
        $bootstatus=$model->getBannerControl($channel);
        $data['show']=$bootstatus;
        //展示服务端广告的或者联盟广告的时候查询库
        //'adtype' =>array('name'=>'adtype','range'=>array(1,2),'require'=>true,'desc'=>'1开机图，2 banner
        $adtype = 2;
        $domain_adcount =new Domainadcount();
        $domain_ad= new Domainad();
        $adsarr = $domain_ad->getList($adtype);
        if (empty($adsarr)) {
            //所有服务端广告关闭了，或者没有服务端广告，直接默认关闭所有广告
            $data['show']=2;
            return $data;
        }
        $total=count($adsarr)-1;
        $adskey=rand(0, $total);
        $list=$adsarr[$adskey];
        $id = $list['ad_id'];
        $data['id']=$id;
        $data['url']  = "http://wxfs.181613.com/index.php?service=V1.Ad.hit&id=$id&adtype=$adtype";
        $imgurl = isset($list['imgkurl']) ? $list['imgkurl'] : '';
        $downurl = isset($list['downurl']) ? $list['downurl'] : '';
        $data['imageurl']=$imgurl;
        //展示后台广告的时候才统计显示数
        if ($id && $data['show']==1) {
            $domain_adcount->addCount($adtype, 1, $imgurl, $id, $downurl);//1展示
        }
        return $data;
    }
    /**
    * @auth chenwengen
    * @data 20180925
    * @description 应用位广告 0关闭所有广告 1展示广告
    *@param $channel 渠道好号
    *@param $versioncode 版本号
    *@param $userid 用户id
    *@param $imei 手机唯一标识码
    *@return url 后台广告url
    *@return imageurl 后台广告图片url
    *@return show 广告类型 0关闭所有广告 1展示后台广告，2展示联盟广告
    **/
    public function postAppControl($channel, $versioncode)
    {
        $data['show']=1;
        //广告轮播时间间隔，单位为毫秒
        $data['time']=60000;
        // $data['time']=2000;
        $data['list']=array(
        array(
          'id'=>0,
          'url'=>'',
          'content'=>'',
          'imageurl'=>'',
          'title'=>'',
          'from'=>'',
          'wxxct'=>false,
        ),
      );

        // 情况2　渠道上线关广告 判断渠道上线配置是否配置了当前版本上线关广告的功能配置了，配置了关闭所有广告
        $offversioncode=$this->getversioncode('type', $channel);
        //当前版本号大等于服务端存储关闭广告的版本号关闭广告 为0关闭所有广告
        if ($versioncode>=$offversioncode || $offversioncode=='0') {
            $data['show']=0;
            return $data;
        }

        // 情况３　查询banner广告控制是否是展示后台广告还是展示联盟广告，告数据一起返回，如果开通联盟广告关闭的话可以展示后台的广告
        //查询后台广告控制情况
        $model=new Modelcontrolad();
        //bootstatus 0关所有广告，1展示服端广告，2展示联盟广告
        $bootstatus=$model->getAppControl($channel);
        $data['show']=$bootstatus;
        //展示服务端广告的或者联盟广告的时候查询库
        //'adtype' =>array('name'=>'adtype','range'=>array(1,2),'require'=>true,'desc'=>'1开机图，2 banner
        $adtype = 4;
        $domain_ad= new Domainad();
        $domain_adcount =new Domainadcount();
        $adsarr = $domain_ad->getList($adtype);
        if (empty($adsarr)) {
            //所有服务端广告关闭了，或者没有服务端广告，直接默认关闭所有广告
            $data['show']=0;
            return $data;
        }
        //随机打乱数据
        shuffle($adsarr);
        foreach ($adsarr as $key=>$ad) {
            //默认不是小程序广告
            $ad['wxxct']=false;
            //默认没有小程序推广来源
            $ad['from']='';
            $content=trim($ad['content']);
            $id = $ad['ad_id'];
            $ad['id']=$id;
            $ad['url']  = "http://wxfs.181613.com/index.php?service=V1.Ad.hit&id=$id&adtype=$adtype";

            //判断是否满足徾信小程序推广格式 xcqid=gh_07781fb5709e&from=1449
            parse_str(htmlspecialchars_decode($content), $params);
            if (isset($params['xcqid']) && isset($params['from']) && trim($params['xcqid'])!='' && $params['from']!='') {
                //是小程序
                $ad['wxxct']=true;
                //将$params['xcqid']赋值给content
                $ad['content']=$params['xcqid'];
                //修改frome
                $ad['from']='?from='.$params['from'];
            }

            $adsarr[$key]=$ad;
            //展示后台广告的时候才统计显示数
            $domain_adcount->addCount($adtype, 1, $ad['imgkurl'], $id, $ad['downurl']);
            //1展示
        }
        //广告列表赋值给返回的数组
        $data['list']=$adsarr;

        //只有华为qudao显示广告
        foreach ($data['list'] as $key=>$ad) {
            if ($ad['ad_id']==50 && $channel!='fenshen_huawei') {
                unset($data['list'][$key]);
                break;
            }
        }
        return $data;
    }

    /**
    * @auth chenwengen
    * @data 20180920
    * @description 通用广告控制 0关闭广告 1展示广告
    *@param $channel 渠道好号
    *@param $ids 已经点击过的广告ID
    *@param $versioncode 版本号id
    *@return show 广告类型 1关闭广告 1展示广告
        **/
    public function postHongbaoControl($channel, $versioncode, $ids)
    {
        $data['id']=0;
        $data['url']='';
        $data['imgurl']='';
        $data['show']=1;
        //复制的文本
        $data['content']='';
        //查询后台广告控制情况
        $model=new Modelcontrolad();
        //bootstatus 0关所有广告，1展示服端广告
        $bootstatus=$model->getHongbaoControl($channel);
        $data['show']=$bootstatus;
        // 渠道上线关广告 判断渠道上线配置是否配置了当前版本上线关广告的功能配置了，配置了关闭所有广告
        $offversioncode=$this->getversioncode($channel);
        //选择了关闭红包广告，判断当前版本号大等于服务端存储关闭广告的版本号关闭广告 为0关闭所有广告
        if ($versioncode>=$offversioncode || $offversioncode=='0') {
            $data['show']=0;
            return $data;
        }
        //展示服务端广告的或者联盟广告的时候查询库
        //'adtype' =>array('name'=>'adtype','range'=>array(1,2),'require'=>true,'desc'=>'1开机图，2 banner
        $adtype = 3;
        $domain_adcount =new Domainadcount();
        $domain_ad= new Domainad();
        $adsarr = $domain_ad->getList($adtype);
        if (empty($adsarr)) {
            //所有服务端广告关闭了，或者没有服务端广告，直接默认关闭所有广告
            $data['show']=0;
            return $data;
        }
        //排除已经点击过的广告
        $ads=array();
        foreach ($adsarr as $ad) {
            if (!in_array($ad['ad_id'], $ids)) {
                //剩余没有点击的广告
                $ads[]=$ad;
            }
        }
        if (empty($ads)) {
            //全部广告都点击了，重新随机所有广告
            $ads=$adsarr;
        }
        $total=count($ads)-1;
        $adskey=rand(0, $total);
        $list=$ads[$adskey];
        $id = $list['ad_id'];
        $data['id']=$id;
        $data['url']  = "http://wxfs.181613.com/index.php?service=V1.Ad.hit&id=$id&adtype=$adtype";
        $imgurl = isset($list['imgkurl']) ? $list['imgkurl'] : '';
        $downurl = isset($list['downurl']) ? $list['downurl'] : '';
        $data['imgurl']=$imgurl;
        //展示后台广告的时候才统计显示数
        if ($id && $data['show']==1) {
            $domain_adcount->addCount($adtype, 1, $imgurl, $id, $downurl);//1展示
        }
        return $data;
    }
}
