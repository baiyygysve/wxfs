<?php
namespace V1\Domain;

use V1\Model\Ad as Modelad;

class Ad
{
    /**
     * 获取广告列表
     * @param $type
     * @return mixed
     */
    public function getList($type)
    {
        $model=new Modelad();
        $list = $model->getList($type);
        return $list;
    }

    /**
     * 获取对应广告内容
     * @param $id
     * @return mixed
     */
    public function findById($id)
    {
        $model=new Modelad();
        $info = $model->findById($id);
        return $info;
    }
}
