<?php
namespace App\Api;

use PhalApi\Api;

/**
 * 常见问题
 *
 * @author: cwg
 */

class Answer extends Api
{
    public function getRules()
    {
        return array(
            'index' => array(

            ),
        );
    }
    public function index()
    {
      $param = array(
          'content' =>"
          <p><strong>1、关于分身微信的简单介绍</strong></p>
          <p style=\"white-space: normal;\">首先，多开出来的微信和原版微信在功能上是没有区别的，小号在独立环境下运行不影响大号。</p><p style=\"white-space: normal;\">其次，我们这个分身微信没有红包、没有外挂、不能加粉，是真的和原版微信是一样的。</p><p><span style=\"\\&quot;font-size:\"><strong>2、<strong style=\"white-space: normal;\">是不是所有的应用都能多开？</strong></strong></span></p><p><span style=\"\\&quot;font-size:\"></span></p><p>目前主流的应用都能实现多开，但是部分app会多开应用进行限制，这部分app相对较少。</p>
          <p><span style=\"\\&quot;font-size:\"><strong>3、<strong style=\"white-space: normal;\">出现闪退、无法多开等情况怎么办？</strong></strong></span></p><p>您可以尝试清理手机进程之后重启软件或者重新下载微信分身，如果还是没能解决问题，请联系客服。</p><p><span style=\"\\&quot;font-size:\"><strong>4、<strong style=\"white-space: normal;\">使用微信分身会被封号吗？</strong></strong></span></p><p>常规使用微信是不会封号的，有违规操作用啥微信都是有封号风险的。</p><p>参与红包赌博、新号频繁和陌生人打招呼、频繁群发消息等，多频次的反常规操作封号就容易封号哦。</p>
          <p><span style=\"\\&quot;font-size:\"><strong>5、如何避免封号？</strong></span></p>
          <p style=\"white-space: normal;\">像用我们的生活号一样的习惯去使用微信小号就能大大的避免封号啦。远离红包赌博、远离外挂，珍爱微信小号。</p>
          <p><strong>6、更多问题请直接联系客服小姐姐</strong></p>
          <p>为了快速的帮您解决问题，麻烦您将手机型号、遇到问题的具体情况一起发给客服哦~</p>
      ",
      );
      	$smarty = new \ctbsea\phalapiSmarty\Lite();
        $smarty->setParams($param);
        $smarty->show();
    }
}
