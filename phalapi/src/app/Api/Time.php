<?php
namespace App\Api;

use PhalApi\Api;

/**
 * 返回系统时间类
 *
 * @author: cwg
 */

class Time extends Api
{
    public function getRules()
    {
        return array(
            'index' => array(

            ),
        );
    }
    /**
     * 返回服务器系统时间戳接口，用户客户端和服务端校对时间
     * @desc 返回服务器系统时间戳
     * @return int time 当前时间戳
     */
    public function index()
    {
        return array(
            'time' => SYS_TIME,
        );
    }
}
