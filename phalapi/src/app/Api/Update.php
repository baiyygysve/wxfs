<?php
namespace App\Api;

use App\Domain\Update as Domainupdate;
use PhalApi\Api;

/**
 * 升级接口
 *
 * @author: cwg
 */


class Update extends Api
{
    public function getRules()
    {
        return array(
            'index' => array(
              'ext' => array('name'=>'ext','type' =>'string','require'=>true,'desc' => '渠道编号','source'=>'post'),
               'versioncode'=>array('name'=>'versioncode','type'=>'int','require'=>true,'desc'=>'升级版本ID','source'=>'post'),
            ),
        );
    }
    /**
     * 升级接口
     * @desc 升级接口
     * @return int type 升级类型0不升级1不强生2强生
     * @return string url 下载地址
     * @return int duration 升级时间间隔，单位秒
     * @return string remark 升级内容
     */
    /*
    public function geturl($partner){
        $p_link =  $this->getORM('developers')->select('url,remark')->where('ext',$partner)->fetchOne();
        return $p_link;
    }

     */
    public function index()
    {
        $domain=new Domainupdate();

        return $domain->getLink($this->ext, $this->versioncode);
    }
}
