<?php
namespace App\Api;

use PhalApi\Api;

/**
 *客服QQ
 *
 * @author: cwg
 */

class Qq extends Api
{
    public function getRules()
    {
        return array(
            'index' => array(

            ),
        );
    }
    /**
     * 客服QQ
     * @desc 返回客服QQ
     * @return string qq 客服QQ
     */
    public function index()
    {
        return array(
            'qq' => '188830926',
        );
    }
}
