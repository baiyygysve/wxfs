<?php
namespace App\Domain;

use App\Model\Update as Updatemodel;
use Curl\Curl;

class Update
{
    public function getLink($ext, $versioncode)
    {
        $returndata=array();
        $updatemodel=new Updatemodel();
        $list=$updatemodel->doSth()->where(array('ext'=>$ext))->fetchOne();
        // if (!$list) {
        //     //不存在记录走官网升级链接接口
        //     $list=$updatemodel->doSth()->where(array('ext'=>'default'))->fetchOne();
        //     // return $list;
        // }
        if (!$list) {
            //不存在记录不升级
            $list=array(
              'type'=>0,
              'url'=>'http://wxfs.181613.com/uploadfile/download/fenshen3.apk',
              'duration'=>2,
              'remark'=>'',
          );
            return $list;
        }
        $updateurl=$list['url'];
        $curl=new Curl();
        $curl->get($updateurl);
        $info=$curl->getInfo();
        $httpstatus=intval($info['http_code']/100);
        if (!in_array($httpstatus, array(2,3))) {
            //url 有问题不升级
            $returndata['type']=0;
            $returndata['url']=$list['url'];
            $returndata['duration']=$list['duration'];
            $returndata['remark']=$list['remark'];
            return $returndata;
        }
        //url 正常判断升级类型
        if ($versioncode>=$list['newcode']) {
            $returndata['type'] = 0;
        } elseif ($versioncode<$list['newcode'] && $versioncode>$list['versioncode']) {
            $returndata['type'] = 1;
        } else {
            $returndata['type'] = 2;
        }
        $returndata['url']=$list['url'];
        $returndata['duration']=$list['duration'];
        $returndata['remark']=$list['remark'];
        return $returndata;
    }
}
