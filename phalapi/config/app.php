<?php
/**
 * 请在下面放置任何您需要的应用配置
 *
 * @license     http://www.phalapi.net/license GPL 协议
 * @link        http://www.phalapi.net/
 * @author dogstar <chanzonghuang@gmail.com> 2017-07-13
 */
if (\PhalApi\DI()->debug) {
    $service_whitelist=array('*.*');
} else {
    $service_whitelist=array(
      'Time.Index',
      'Answer.Index',
      'Ad.Hit',
      'Qq.Index',
    );
}

return array(

    /**
     * 应用接口层的统一参数
     */
    'apiCommonRules' => array(
      'sign' => array('name' => 'sign', 'require' => true,'desc'=>'安全校证码','source'=>'post'),
      'timestamp' => array('name' => 'timestamp', 'require' => true,'desc'=>'时间戳','source'=>'post'),
    ),

    /**
     * 接口服务白名单，格式：接口服务类名.接口服务方法名
     *
     * 示例：
     * - *.*         通配，全部接口服务，慎用！
     * - Site.*      Api_Default接口类的全部方法
     * - *.Index     全部接口类的Index方法
     * - Site.Index  指定某个接口服务，即Api_Default::Index()
     */
    // 'service_whitelist' => array(
    //     //'Site.Index',
    // ),
    //
    'service_whitelist'=>$service_whitelist,
     //md5加密盐
    'salt'=>'8FP4t',
    //url生成过期时间间隔
    'expirationTime'=>5,

);
